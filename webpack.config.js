/**
 * Created by tiennd on 04/11/2016.
 */

var webpack = require('webpack');
var path = require('path');

var BUILD_DIR = path.resolve( __dirname, 'src/client/public');
var APP_DIR = path.resolve(__dirname, 'src/client/app');

var plugins = [];
plugins = plugins.concat([
    new webpack.optimize.UglifyJsPlugin(),
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.optimize.DedupePlugin(),
]);

var config = {
    entry: APP_DIR + '/index.js',
    output: {
        path: BUILD_DIR,
        filename: 'bundle.js',
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                query: {
                    presets: ['react', 'es2015']
                }
            }
        ]
    },
    // plugins: plugins

};


module.exports = config



